<h3 align="center">
    <img alt="Logo" title="#logo" width="300px" src="https://lh3.googleusercontent.com/HOnppISRn2zCbaePkXR2ynOQM26kyz8GgtGAjV2gjHHqECtlTHzgSiQK6_D9rniYtMbFbKLJWuHb31Y1-qviPKPwh6U=s9999">
    <br><br>
    <b>Recicle! Ajude o meio ambiente!</b> 
</h3>

<p align="center">
  <a href="https://rocketseat.com.br">
    <img alt="Made by Rocketseat" src="https://img.shields.io/badge/made%20by-Rocketseat-%237519C1">
  </a>
  <a>
</p>


<a id="sobre"></a>

## :bookmark: Sobre

O <strong>Ecoleta</strong> é uma aplicação Web e Mobile para ajudar pessoas a encontrarem pontos de coleta para reciclagem.

Essa aplicação foi construída na trilha <strong>Booster</strong> da <strong>Next Level Week</strong> distribuída pela [Rocketseat](https://rocketseat.com.br/). A ideia de criar uma aplicação voltada ao meio ambiente surgiu da coincidência da data do curso e a data da <strong>semana do meio ambiente</strong>

<a id="como-usar"></a>

## :fire: Como usar

- ### **Pré-requisitos**

  - Possuir o **[Node.js](https://nodejs.org/en/)** instalado na máquina
  - Ter um gerenciador de pacotes seja o **[NPM](https://www.npmjs.com/)** ou **[Yarn](https://yarnpkg.com/)**.
  - Por fim, é **essencial** ter o **[Expo](https://expo.io/)** instalado de forma global na máquina para fazer o start com mobile

1. Faça um clone :

```sh
  $ git clone https://gitlab.com/moroleandro/ecoleta.git
```

2. Executando a Aplicação:

```sh
  # Instale as dependências
  $ npm install

  ## Crie o banco de dados
  $ cd api
  $ echo "PG_CONNECTION=[URL-BANCO-POSTGRES]" > .env
  $ npm run knex:migrate
  $ npm run knex:seed
  $ npm run dev

  # Aplicação web
  $ cd web
  $ npm start

  # Aplicação mobile
  $ cd mobile
  $ npm start
```

---

<h4 align="center">
    Feito por <a href="https://www.linkedin.com/in/moroleandro/" target="_blank">Leandro Moro</a>
</h4>
