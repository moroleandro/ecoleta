import * as dotenv from 'dotenv';
import path from 'path';

dotenv.config();

module.exports = {
    client: 'pg',
    connection: process.env.PG_CONNECTION,
    searchPath: ['knex', 'public'],
    migrations: {
        directory: path.resolve(__dirname, 'src', 'database', 'migrations')
    },
    seeds: {
        directory: path.resolve(__dirname, 'src', 'database', 'seeds')
    },
    useNullAsDefault: true
};