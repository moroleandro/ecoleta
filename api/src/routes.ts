import express from 'express';

import ItemsController from './controller/ItemsController';
import PointsController from './controller/PointsController';

const routes = express.Router();
const itemsController = new ItemsController();
const pointsController = new PointsController();

routes.get('/', (request, response) => {
    return response.json({message: `API ECOLETA - v${require('../package.json').version}`})
});

routes.get('/items', itemsController.index);
routes.get('/points/:id', pointsController.show);
routes.get('/points', pointsController.index);
routes.post('/points', pointsController.create);

export default routes;