import knex from 'knex';
import * as dotenv from 'dotenv';

dotenv.config();

const connection = knex({
    client: 'pg',
    connection: process.env.PG_CONNECTION,
    searchPath: ['knex', 'public'],
    useNullAsDefault: true,
});

export default connection;